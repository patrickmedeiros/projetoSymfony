<h1>Iniciando instalação e gerando CRUD no framework Symfony</h1>

<h2>Framework Symfony</h2>

<p>Symfony é um conjunto de componentes PHP reutilizáveis e uma estrutura PHP para projetos da web.</p>

<p>Acelerar a criação e manutenção de seus 
aplicativos da Web PHP. Encerre tarefas repetitivas
de codificação e aproveite o poder de controlar seu código.</p>

<h2>Objetivos: </h2>
<p>Conhecer sobre o framework, configurar banco de dados, gerar CRUD básico, aprendendo sobre a ferramenta.</p>

<h2>Link dos vídeos: </h2>

<h3>Instalando, configurando e gerando a primeira View:</h3>
https://youtu.be/kzTImpfz4yw

<h3> Gerando CRUD:</h3>
https://www.youtube.com/watch?v=xR5-6ShBGzI&t=120s.